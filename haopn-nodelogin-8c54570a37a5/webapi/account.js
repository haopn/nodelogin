const connection = require('../config/database');
var router = express.Router();
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var path = require('path');
var uuid = require('uuid/v4');
var md5 = require('md5');
var ejs = require('ejs');
const multer = require("multer");
const dateFormat = require('dateformat');

// Gui tep de hien thi login.html cho khach hang

router.get('/', function(req,res){
    console.log(__dirname);
    res.render(path.join(__dirname, '../' , '/views/login.html'),{'error':false});
});

router.get('/register', function(req,res){
    res.render(path.join(__dirname, '../' , '/views/register.html'),{'error':false});
});

router.get('/forgotpass', function(req,res){
    res.render(path.join(__dirname, '../' , '/views/forgotpass.html'),{'error':false});
});

router.get('/profile', function(req,res){
    var selectUrl = `SELECT url_image FROM accounts WHERE username = "${req.session.username}"`
    connection.query(selectUrl, function(error,result,fields){
        if (error) {
            console.error("SELECT accounts error", error)
            res.send(error);
        }else{
        url_image = result[0].url_image
        res.render(path.join(__dirname, '../' , '/views/profile.html'),{'error':false,'username':req.session.username, 'url': url_image});
    }
    })
});

router.get('/upload', function(req,res){
    var selectUrl = `SELECT url_image FROM accounts WHERE username = "${req.session.username}"`
    connection.query(selectUrl, function(error,result,fields){
        if (error) {
            console.error("SELECT accounts error", error)
            res.send(error);
        }else{
        url_image = result[0].url_image
        res.render(path.join(__dirname, '../' , '/views/upload.html'),{'error':false,'username':req.session.username, 'url': url_image});
    }
    })
});

router.post('/auth', function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    var hash = crypto.createHash('md5').update(password).digest('hex');
    if (username && password){
        connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [username, hash], function(error, result, fields){
            if (result.length > 0){
                req.session.loggedin = true;
                req.session.username = username;
                res.redirect('/account/home');
            }else {
                res.render(path.join(__dirname, '../' , '/views/login.html'),{'message':'Incorect Password or Username','error':true});
            } 
        });
    } else {
        res.send('Please enter Username and Password! '); 
    }
});

router.get('/home', function(req, res) {
    var username = req.session.username;
	if (req.session.loggedin) {
        connection.query('SELECT username, email, DATE_FORMAT(date_time,"%m/%d/%Y %H:%i") FROM accounts', function(error, result, fields){
            // for (var i = 0; i < result.length; i++){
            //     time = result[i].datetime;
            //     var format = (dateFormat(time, "dd-mm-yyyy HH:MM"));
            //      console.log(format[i]);  
            
            // var format = (dateFormat(result.datetime, "dd-mm-yyyy HH:MM"));
            res.render(path.join(__dirname , '../' , '/views/home.html'), {"name":username,"data":result}); 
        })
	} else {
		res.render(path.join(__dirname, '../' , '/views/login.html'),{'message':'Please login to view this page','error':true});
    }  
});

router.post('/register', function (req, res) {
    var re_username = req.body.re_username;
    var re_password = req.body.re_password;
    var re_repeatpassword = req.body.re_repeatpassword;
    var re_email = req.body.email;
    date = (dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss"));
    
    // var date = new Date();
    var hash = crypto.createHash('md5').update(re_password).digest('hex');
    
    var InsertData = `INSERT INTO accounts(id, username, password, email) VALUES ("${uuid()}", "${re_username}", "${hash}", "${re_email}")`
    
    
        if (re_username && re_password && re_email && re_repeatpassword && re_password === re_repeatpassword){
        connection.connect()
        connection.query(InsertData, function(error, result, fields){
        if (error) {
            console.error("INSERT accounts error", error)
            res.send(error);
        }else{
            //console.log(result);
            res.render(path.join(__dirname, '../' , '/views/register.html'),{'message':'Register Account Successfully','error1':true});
    }
         //connection.end()
    });
        }else{  
        res.render(path.join(__dirname, '../' , '/views/register.html'),{'message':'Incorect Password','error':true});
        // res.end();
    }
});

router.post('/forgot', function (req, res) {
    var forgot_email = req.body.forgot_email;
    // var forgot_pass = req.body.forgot_pass;
    if (forgot_email){
        connection.query('SELECT password FROM accounts WHERE email = ?', [forgot_email], function(error, result, fields){
            if (error || result.length <= 0) {
                // console.log__dirname, '../..', 'foo.bar');
                res.render(path.join(__dirname, '../' , '/views/forgotpass.html'),{'message':'Email not found','error2':true});
            }else{
                console.log(result);
                req.session.confirm = true;
                var randompass = Math.random().toString(36).substr(2, 10);
                var hashnewpass = crypto.createHash('md5').update(randompass).digest('hex');
                connection.query('UPDATE accounts SET password = ? WHERE email = ?', [hashnewpass, forgot_email])
                // req.session.password = result[0].password;
                
                res.render(path.join(__dirname, '../' , '/views/forgotpass.html'), {'message':forgot_email,'error3':true});
                
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                           user: 'haopn@fimplus.vn',
                           pass: 'anhtuyeth'
                       }
                   });
                
                const mailOptions = {
                    from: 'haopn@fimplus.vn', // sender address
                    to: forgot_email, // list of receivers
                    subject: 'Password of email:' + forgot_email, // Subject line
                    html: '<p>You have got a new message</b><ul><li>Email:' + req.body.forgot_email + '</li><li>Password:' + randompass + '</li></ul>'// plain text body
                  };

                  transporter.sendMail(mailOptions, function(err, info){
                    if (err) {
                        console.log(err);
                        
                    } else {
                        console.log('Message sent: ' +  info.response);
                       
                    }
                });
                // res.render(path.join(__dirname + '/forgotpass.html'),{'message':'Register Account Successfully','error1':true});
        }
            // connection.end()
        });
    } else {
        res.send('Please enter Username and Password! '); 
    }
});

  
router.post('/profile', function (req, res) {
    //res.render(path.join(__dirname, '../' , '/views/profile.html'),{'name':username});
    var fullname = req.body.fullname;
    var phone = req.body.phone;
    var username = req.session.username;
    
    var UpdateData = `UPDATE accounts SET fullname = ("${fullname}"), phone = (${phone}) WHERE username = ("${username}")`
    if(req.session.loggedin && fullname && phone){
        //connection.connect()
        // upload.single('myImage'), (req, res, next) => {
        //     var file = req.file
        //     if (!file) {
        //       var error = new Error('Please upload a file')
        //       error.httpStatusCode = 400
        //       return next(error)
        //     }else{
        //     // {"fieldname":"myImage","originalname":"62099219_1354052051414766_9045662757050908672_n.jpg","encoding":"7bit","mimetype":"image/jpeg","destination":"./upload","filename":"myImage-1562317110118.png","path":"upload\\myImage-1562317110118.png","size":89449}
        //     var dataImg = JSON.parse(file)
        //     url = dataImg.destination + "/" + filename
        
        //     res.send("Success upload")
        //     }
        //   }     
        connection.query(UpdateData, function(error, result, fields){
        if (error) {
            console.error("Update accounts error", error)
            res.send(error);
        }else{
            //console.log(result);
            res.render(path.join(__dirname, '../' , '/views/profile.html'),{'message':'Update Account Successfully','username':username,'error1':true});
    }
        // connection.end()
    });
        }else{  
        res.send('Please login to view this page!');
    }
});

// Tao noi luu tru upload
var storage = multer.diskStorage({
    destination: function(req,res,cb){
        cb(null, './upload')
    },
    filename: function(req,file,cb){
        cb(null, file.fieldname + '-' + Date.now()+ ".png")
    }
})

var upload = multer ({storage:storage})
router.post('/upload', upload.single('myImage'), (req, res, next) => {
    var file = req.file
    var dataImg = file
    var url = dataImg.filename
    var username = req.session.username;

    var InsertUrl = `UPDATE accounts SET url_image = ("${url}") WHERE username = ("${username}")`
    if (!file) {
      var error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }else{
        connection.query(InsertUrl, function(error, result, fields){
            if (error) {
                console.error("Upload image error", error)
                res.send(error);
            }else{
                //console.log(result);
                res.render(path.join(__dirname, '../' , '/views/upload.html'),{'message':'Upload image Successfully','error1':true,'username':username, 'url':url});
        }
             //connection.end()
        });
    } 
  })

  router.post('/search', function (req, res) {
    var searchdata = req.body.searchdata;
    var username = req.session.username;
    var SearchDb = `SELECT username, email, DATE_FORMAT(date_time,"%m/%d/%Y %H:%i") FROM accounts WHERE username LIKE ("%${searchdata}%")`
    if (req.session.loggedin && searchdata){
        connection.query(SearchDb, function(error, result, fields){
            if (error) {
            }else{
                res.render(path.join(__dirname, '../' , '/views/home.html'),{'message':'Search Successfully','name':username,'data':result,'error1':true});
        }
            // connection.end()
        });
            }else{ 
            res.send('Please login to view this page!');
        }
});

module.exports = router