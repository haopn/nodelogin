const router = express.Router()
//account
const account = require('./account');
app.use("/account", account);

const product = require('./product');
app.use("/product", product)

module.exports = router