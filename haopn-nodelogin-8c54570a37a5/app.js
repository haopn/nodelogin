express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');


// Ket noi voi database
// var connection = require('../config/database')

 app = express();
 app.use(express.static('upload'))
// Express session
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

// Body parser
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

var engine = require('consolidate');

app.use(express.static(path.join(__dirname, 'views')));

app.set('views', __dirname + '/nodelogin');
app.engine('html', engine.mustache);
app.set('view engine', 'html');

require('./webapi')
// config mail server

app.listen(3001);